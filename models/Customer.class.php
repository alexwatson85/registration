<?php
    class Customer {

        public $customerId; 
        public $firstname; 
        public $lastname;
        public $tel;
        public $street;
        public $housenumber;
        public $zip;
        public $city;
        public $ownerName;
        public $iban;
        public $paymentDataId;
        
        function getCustomerID(){
            return $this->customerId;
        }
    }
?>