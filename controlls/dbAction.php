<?php
require_once("../models/Customer.class.php");
require_once('config.php');  
session_start();

$customer = new Customer();
$customer->firstname = $_SESSION['firstname'];
$customer->lastname = $_SESSION['lastname'];
$customer->tel = $_SESSION['tel'];
$customer->street = $_SESSION['street'];
$customer->housenumber = $_SESSION['housenumber'];
$customer->zip = $_SESSION['zip'];
$customer->city = $_SESSION['city'];
$customer->ownerName = $_SESSION['ownerName'];
$customer->iban = $_SESSION['iban'];

$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABSE);
if ($conn->connect_errno) {
    die("Verbindung fehlgeschlagen: " . $conn->connect_error);
}  
$query = "INSERT INTO customer (firstname,lastname,tel,street,housenumber,zip,city,ownerName,iban) VALUES (".
"'" . $customer->firstname . "'," .
"'" . $customer->lastname . "'," .
"'" . $customer->tel . "'," .
"'" . $customer->street . "'," .
"'" . $customer->housenumber . "'," .
"'" . $customer->zip . "'," .
"'" . $customer->city . "'," .
"'" . $customer->ownerName . "'," .
"'" . $customer->iban . "')";

$conn->query($query);
$customer->customerId = $conn->insert_id;


// {
// “customerId”: 1,
// “iban”: "DE8234",
// “owner”: "Max Mustermann"
// }

$url = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";

$data = array(
  'customerId' => "'".$customer->customerId."'",
  'iban' => "'".$customer->iban."'",
  'owner' => "'".$customer->ownerName."'",
);

$content = json_encode($data);
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HEADER, false);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

$json_response = curl_exec($curl);

$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

curl_close($curl);

$response = json_decode($json_response, true);

foreach ($response as &$value) {
    $customer->paymentDataId = $value;
}

$updateQuery = "UPDATE customer set paymentDataId = '" . $customer->paymentDataId . "' WHERE id=".$customer->customerId;

$conn->query($updateQuery);

$_SESSION["paymentDataId"] = $customer->paymentDataId;
$conn->close();



header("Location:http://localhost/registration/registration/");


?>