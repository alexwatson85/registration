<?php
require_once("../models/Customer.class.php");
session_start(); 

$customer = new Customer();

$customer->firstname = $_POST['firstname'];
$customer->lastname = $_POST['lastname'];
$customer->tel = $_POST['tel'];

if((isset($customer->firstname) OR !empty($customer->firstname)) 
AND (isset($customer->lastname) OR !empty($customer->lastname))
AND (isset($customer->tel) OR !empty($customer->tel))) {

    $_SESSION['firstname'] = $customer->firstname;
    $_SESSION['lastname'] = $customer->lastname;
    $_SESSION['tel'] = $customer->tel;
    
}

$customer->street = $_POST['street'];
$customer->housenumber = $_POST['housenumber'];
$customer->zip = $_POST['zip'];
$customer->city = $_POST['city'];

if((isset($customer->street) OR !empty($customer->street)) 
AND (isset($customer->housenumber) OR !empty($customer->housenumber))
AND (isset($customer->zip) OR !empty($customer->zip))
AND (isset($customer->city) OR !empty($customer->city))) {

    $_SESSION['street'] = $customer->street;
    $_SESSION['housenumber'] = $customer->housenumber;
    $_SESSION['zip'] = $customer->zip;
    $_SESSION['city'] = $customer->city;
    
}

$customer->ownerName = $_POST['ownerName'];
$customer->iban = $_POST['iban'];

if((isset($customer->ownerName) OR !empty($customer->ownerName)) 
AND (isset($customer->iban) OR !empty($customer->iban))) {

    $_SESSION['ownerName'] = $customer->ownerName;
    $_SESSION['iban'] = $customer->iban;
    $_SESSION['customer'] = $customer;   
}

header("Location:http://localhost/registration/registration/");

?>