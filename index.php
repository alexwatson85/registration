<!DOCTYPE html>  
 <html lang="en">  
    <head>  

    </head>  
    <body>                  
        <header>  
                <h1>Registration</h1>  
        </header>  
        <?php
            session_start();
            if(!isset($_SESSION['firstname']) OR empty($_SESSION['firstname'])) {

                include "views/personalInfo.php"; 
            }
            else if(!isset($_SESSION['street']) OR empty($_SESSION['street'])) {

                include "views/adressInfo.php"; 
            }
            else if(!isset($_SESSION['iban']) OR empty($_SESSION['iban'])) {

                include "views/bankInfo.php"; 
            }
            else{

                include "views/overview.php"; 
            }
        ?>
    </body>  
</html>  